var express = require('express');
var app = express();
const Promise = require('bluebird');
const path = require('path');
const formidable = require('express-formidable')
const fs = require("fs");
const readFile = Promise.promisify(require("fs").readFile);
const http =require('http').Server(app)
const io = require('socket.io')(http)
io.sockets.setMaxListeners(0);
const csvparser= require('./modules/csvparser.js')(app,io);
process.setMaxListeners(0);
app.set('view engine', 'pug');

app.use(formidable())
app.use(express.static('public'));
app.get('/', function(req, res){
	res.render('index', { title: 'Hey', message: 'Hello there!'});
})

app.get('/public/my.csv', function(req,res){
	var file = __dirname + '/public/my.csv';
	res.download(file);
});

app.post('/upload', function(req,res){
	res.render('result',{});
	var uploadDir = path.join(__dirname, '/uploads') + '/' +req.files.file.name;
	io.on('ready',function(msg){
		io.emit('url recive','ready');
	});
	readFile(req.files.file.path).then(function(content){
		  fs.writeFile(uploadDir, content, function (err) {
    		csvparser.check(uploadDir,io);
 		 });
	}). catch(function (e){
		io.emit('url recive','ooops');
		console.log(e);
	});
})

 var server = http.listen(8081, function () {
 var host = server.address().address
 var port = server.address().port
   
   console.log("Example app listening at http://%s:%s", host, port)
})