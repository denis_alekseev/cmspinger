var cheerio = require('cheerio');
const request = require('prequest');
var exports = module.exports = {};
module.exports = function(io)
{
    return {
    detectCms: function(url,callback){
/*    	request({method: 'GET', url:url, rejectUnauthorized: false}).then(function(body){
            res = '';*/
         	//$ = cheerio.load(body);
            isDrupal(url,function(url,system,version){
                io.emit('url recive',url + " " + system + " " + version +"<br />");
                callback(null,{"URL":url,"system":system,"version":version})
            });
         	/*if(res = isDrupal($,url) , res.status !== false)
         	{
                io.emit('url recive',url + " " + res.system + " " + res.version +"<br />");
    			callback(null,{"URL":url,"system":res.system,"version":res.version})
         	}
         	else
            if(res = isJoomla($) , res.status !== false)
         	{
                io.emit('url recive',url + " " + res.system + " " + res.version +"<br />");
    			callback(null,{"URL":url,"system":res.system,"version":res.version})
         	}
         	else if(res = isMagento($), res.status !== false)
         	{
                io.emit('url recive',url + " " + res.system + " " + res.version +"<br />");
         		callback(null,{"URL":url,"system":res.system,"version":res.version})	
         	}  
            else if(res = isMediaWiki($), res.status !== false)
            {
                io.emit('url recive',url + " " + res.system + " " + res.version +"<br />");
               callback(null,{"URL":url,"system":res.system,"version":res.version}) 
            }
            else if(res = isMoodle($), res.status !== false)
            {
                io.emit('url recive',url + " " + res.system + " " + res.version +"<br />");
               callback(null,{"URL":url,"system":res.system,"version":res.version}) 
            }
            else if(res = isOpenCart($), res.status !== false)
            {
                io.emit('url recive',url + " " + res.system + " " + res.version +"<br />");
               callback(null,{"URL":url,"system":res.system,"version":res.version}) 
            }
            else if(res = isPhpMyAdmin($), res.status !== false)
            {
                io.emit('url recive',url + " " + res.system + " " + res.version +"<br />");
               callback(null,{"URL":url,"system":res.system,"version":res.version}) 
            }      
            else if(res = isWordpress($), res.status !== false)
            {
                io.emit('url recive',url + " " + res.system + " " + res.version +"<br />");
               callback(null,{"URL":url,"system":res.system,"version":res.version}) 
            } 
         	else
            {
                io.emit('url recive',url + " " + res.system + " " + res.version +"<br />");
         		callback(null,{"URL":url,"system":'Unknown',"version":'Unknown'})
            }*/
/*       		})
            .catch(function(err){
              io.emit('url recive',url + " Not found<br />");
//              console.log(url);
//              console.log(err);
              callback(null,{"URL":url,"system":'host not found',"version":'Unknown'})  
            })*/
    	}
    }

function DrupalVersion(url,cb) {
/*            if (/jQuery.extend\(Drupal.settings/.test($('head').html())) {
                var directory = $('head').html().match(/jQuery.extend\(Drupal.settings, *{ *"*basePath"*: *"([a-z0-9\\\\\/\-_.~]*)"/i);
                if(directory != null ) {
                    directory = directory[1];
                    directory = directory.replace(/\\u002F/g,"/");
                    directory = directory.replace(/\\/g,"");
                }
            }
            else if (/<link [type="text\/css" ]*rel="stylesheet" [media="all" ]*href="(https?:\/\/[a-z0-9\-_.~]+\/|\/)[a-z0-9\-_.~\/]+sites\/(default\/files|all\/modules|all\/themes)\//i.test($('head').html()))
                var directory = "/"+$('head').html().match(/<link [type="text\/css" ]*rel="stylesheet" [media="all" ]*href="(https?:\/\/[a-z0-9\-_.~]+\/|\/)([a-z0-9\-_.~\/]+)sites\/(default\/files|all\/modules|all\/themes)\//i)[2];
            else if (/@import url\("(https?:\/\/[a-z0-9\-_.~]+\/|\/)[a-z0-9\-_.~\/]+sites\/(default\/files|all\/modules|all\/themes)\//i.test($('head').html()))
                var directory = "/"+$('head').html().match(/@import url\("(https?:\/\/[a-z0-9\-_.~]+\/|\/)([a-z0-9\-_.~\/]+)sites\/(default\/files|all\/modules|all\/themes)\//i)[2];
            else if (/<style type="text\/css" media="all">@import "(https?:\/\/[a-z0-9\-_.~]+\/|\/)[a-z0-9\-_.~\/]+sites\/(default\/files|all\/modules|all\/themes)\//i.test($('head').html()))
                var directory = "/"+$('head').html().match(/<style type="text\/css" media="all">@import "(https?:\/\/[a-z0-9\-_.~]+\/|\/)([a-z0-9\-_.~\/]+)sites\/(default\/files|all\/modules|all\/themes)\//i)[2];
            else*/
                var directory = "/";

            request(url + directory + 'CHANGELOG.txt')
                .then(function (body){
                    var version = body.match(/Drupal ([0-9.]+),/)[1];
                    console.log("Version: " + version);
                    cb(url,'Drupal', version);
                })
                .catch(function (err) {
                   request(url + directory + 'core/misc/ajax.js').then(function(body){
                    if (/Drupal\.behaviors\.AJAX/.test(body)) {
                        if (/if \(ajaxSettings\)/.test(body))
                            cb(url,'Drupal', '8.2.0-8.2.1');
                            //console.log("version: 8.2.0-8.2.1");
                        else
                            cb(url,'Drupal', '8.0.0-8.1.10');
                            //console.log("version: 8.0.0-8.1.10");
                    }
                    else
                    {
                        throw "drupal 7"
                    }
                   })
                   .catch(function(err){
                         request(url + directory + 'misc/ajax.js').then(function(body){
                            if (/Drupal\.ajax/.test(body)) {
                                if (/Drupal\.displayAjaxError/.test(body))
                                {
                                    cb(url,'Drupal', '7.51');
                                }
                                else
                                {
                                    cb(url,'Drupal', '7.0-7.50');                                    
                                }
                            }
                            else
                                throw "drupal 6";
                         })
                         .catch(function(err){
                            request(url + directory + 'modules/system/system.js').then(function(body){
                                if (/clean\-url\.clean\-url\-processed/.test(body)) {
                                    if (/\/\\\?\//.test(body))
                                        cb(url,'Drupal', '6.38');
                                    else
                                        cb(url,'Drupal', '6.0-6.37');
                                }
                            });
                         })
                   })
                })     
}
function isDrupal(url,cb)	{
/*     	generator = $('meta[name=generator]').attr('content');
     	status = false;
     	system = '';
     	version = '';
     	if(generator !== undefined )
     		{
     			if(generator.includes('Drupal 7'))
     			{
     				system = 'Drupal';
     				version = 7;
     				status = true;
     			}
     			else if(generator.includes('Drupal 8'))
     			{
     				system = 'Drupal'
     				version = 8;
     				status = true;
     			}
     		}
   			else
   			{
     			if ( (/jQuery.extend\(Drupal.settings/.test($('head').html()))
     	 		|| (/<link [type="text\/css" ]*rel="stylesheet" [media="all" ]*href="[a-z0-9\:\\\\\/\-_.~]*\/sites\/(default\/files|all\/modules|all\/themes)\//i.test($('head').html()))
     	 		|| (/@import url\("[a-z0-9\:\\\\\/\-_.~]*\/sites\/(default\/files|all\/modules|all\/themes)\//i.test($('head').html()))
     	 		|| (/<style type="text\/css" media="all">@import "[a-z0-9\:\\\\\/\-_.~]*\/sites\/(default\/files|all\/modules|all\/themes)\//i.test($('head').html())))
     			{
     				system = 'Drupal';
     				version = 'Unknown';
     				status = true;
     			}
   			}
   			if(status == true)
            {*/
                
                version = DrupalVersion(url,cb);
   				//return {"status":status,"system":system,"version":version}
/*            }
   			else
   				return {"status": false}
*/
}

function isJoomla($) {
		generator = $('meta[name=generator]').attr('content');
     	status = false;
     	system = '';
     	version = '';
     	if(generator !== undefined )
     		{
     			if(generator.includes('Joomla!'))
     			{
     				if(generator.includes('Joomla! - Copyright')){
     				system = 'Joomla';
     				version = 1;
     				status = true;
     			}
     			else if(/Joomla! 1.[567]/.test(generator))
     			{
     				system='Joomla';
     				version = generator.match(/Joomla! (1.[567])/)[1];
     				status = true; 
     			}
     			else if(/Joomla! - Open Source Content Management/.test(generator))
     			{
       				system='Joomla';
     				version = 'Unknown';
     				status = true;    				
     			}
     		}
   			else
   			{
     			if ( (/<script [type="text\/javascript" ]*src="[a-z0-9\:\\\\\/\-_.~]*\/(media\/system\/js\/|components\/com_|modules\/mod_)/i.test($('head').html()))
     			 || (/<link [type="text\/css" ]*rel="stylesheet" href="[a-z0-9\:\\\\\/\-_.~]*\/(media\/system\/js\/|components\/com_|modules\/mod_)/i.test($('head').html()))
     			 || (/<link rel="stylesheet" href="[a-z0-9\:\\\\\/\-_.~]*\/t3\-assets\//i.test($('head').html())))
     			{
     				system = 'Joomla';
     				version = 'Unknown';
     				status = true;
     			}
   			}
   			if(status == true)
   				return {"status":status,"system":system,"version":version}
   			else
   				return {"status": false}

		}
	}

	function isMagento($)
	{
		status = false;
     	system = '';
     	version = '';
		if(/<link [rel="stylesheet" type="text\/css"]*[rel="icon"]* href="https?:\/\/[a-z0-9\-_.~]+[a-z0-9\-_.~\/]*\/skin\/frontend\//i.test($('head').html())
		 || /<link (rel="icon"|rel="shortcut icon") href="https?:\/\/[a-z0-9\-_.~]+[a-z0-9\-_.~\/]*\/media\/favicon\/default\//i.test($('head').html())
		 || /Mage.Cookies.path *= *'\//i.test($('head').html())
		 || /['|"]mage\/translate['|"]/i.test($('head').html()))
		{
     				system = 'magento';
     				version = 'Unknown';
     				status = true;			
		}
		if(status == true)
   			return {"status":status,"system":system,"version":version}
   		else
   			return {"status": false}
	}

    function isMediaWiki($)
    {
        status = false;
        system = '';
        version = ''; 
        if(/<meta name="generator" content="MediaWiki [0-9a-z\.\-\+]+/i.test($('head').html())) 
        {
                system = 'MediaWiki';
                version = 'Unknown';
                status = true;
        }

        if(status == true)
            return {"status":status,"system":system,"version":version}
        else
            return {"status": false}        
    }
    function isMoodle($)
    {
        status = false;
        system = '';
        version = ''; 
        if(/"group":"moodle"/i.test($('head').html())
         || /<script type="text\/javascript" src="[a-z0-9\:\\\\\/\-_.~]*\/lib\/overlib\/overlib_cssstyle.js"><\/script>[\n\r]+<script type="text\/javascript" src="[a-z0-9\:\\\\\/\-_.~]*\/lib\/cookies.js"><\/script>[\n\r]+<script type="text\/javascript" src="[a-z0-9\:\\\\\/\-_.~]*\/lib\/ufo.js"><\/script>/i.test($('head').html()))
        {
                system = 'Moodle';
                version = 'Unknown';
                status = true;
        }

        if(status == true)
            return {"status":status,"system":system,"version":version}
        else
            return {"status": false}        
    }
    function isOpenCart($)
    {
        status = false;
        system = '';
        version = ''; 
        if(/catalog\/view\/(javascript|theme)\//i.test($('head').html())) 
        {
                system = 'OpenCart';
                version = 'Unknown';
                status = true;
        }

        if(status == true)
            return {"status":status,"system":system,"version":version}
        else
            return {"status": false}        
    }

    function isPhpMyAdmin($)
    {
         status = false;
        system = '';
        version = ''; 
        if(/\| phpMyAdmin [0-9.]+<\/title>/i.test($('head').html()))
        {
                system = 'phpMyAdmin';
                version = $('head').html().match(/\| phpMyAdmin ([0-9.]+)<\/title>/i)[1];
                status = true;
        }
        else if(/<title>phpMyAdmin [0-9.]+ \-/i.test($('head').html()))
        {
                system = 'phpMyAdmin';
                version = $('head').html().match(/<title>phpMyAdmin ([0-9.]+) \-/i)[1];
                status = true;            
        }
        else if(/<link rel=\"stylesheet\" type=\"text\/css\" href="phpmyadmin.css.php/.test($('head').html()))
        {
                system = 'phpMyAdmin';
                version = 'Unknown';
                status = true;            
        }
        
        if(status == true)
            return {"status":status,"system":system,"version":version}
        else
            return {"status": false}        
    }

    function isWordpress($)
    {
        status = false;
        system = '';
        version = ''; 
        if(/<meta name="generator" content="WordPress [0-9\.]+/i.test($('head').html())) 
        {
                system = 'Wordpress';
                version = $('head').html().match(/<meta name="generator" content="WordPress ([0-9\.]+)/i)[1];
                status = true;
        }
        else if(/<meta name="generator" content="WordPress [0-9\.]+/i.test($('head').html())  && !/<meta name="generator" content="WordPress\.com"/i.test($('head').html()))
         {
                system = 'Wordpress';
                version = 'Unknown';
                status = true;            
         }
        if(status == true)
            return {"status":status,"system":system,"version":version}
        else
            return {"status": false}         
    }
}