var csv = require("fast-csv");
const fs =  require('fs')
var async = require('async');

module.exports = function(app,io)
{
	var check = require('./detector.js')(io)
	var urls = [];
	return {
		check:function(path) {
			csv.fromPath(path, {headers : true})
		.on("data", function(data){
			 url = data.URL;
			 if(!url.includes('http'))
			 	url = 'http://' + url
			 urls.push(url);
		 })
		 .on("end", function(){
		 	async.mapLimit(urls,500,check.detectCms,function(err,item){
		 		//console.log(item);
		 		csv
		   			.writeToPath("public/my.csv", item, {headers: true})
		   			.on("finish", function(){
       					io.emit('url recive','<a href="/public/my.csv">Download File</a>');
  					})
		   			;
		 		});
		 });

		}
	}
}